## Національний технічний університет України<br>“Київський політехнічний інститут ім. Ігоря Сікорського”

## Факультет прикладної математики<br>Кафедра системного програмування і спеціалізованих комп’ютерних систем


# Лабораторна робота №1<br>"Базова робота з git"

## КВ-13 Яцков Максим

## Хід виконання роботи

### 1. Зклонувати будь-який невеликий проєкт open-source з github

#### 1) Зклонувати двічі: повний репозиторій, а також частковий, що міститиме лише один коміт однієї гілки за вашим вибором.

```
$ git clone https://github.com/EdoardoTosin/xTetris-Game   
Клонування в "xTetris-Game"..  
remote: Enumerating objects: 1563, done.  
remote: Counting objects: 100% (1131/1131), done.  
remote: Compressing objects: 100% (671/671), done.  
remote: Total 1563 (delta 535), reused 1035 (delta 439), pack-reused 432  
Отримання об’єктів: 100% (1563/1563), 3.08 МіБ | 949.00 КіБ/с, готово.  
Розв’язання дельт: 100% (873/873), готово.  
$ git clone https://github.com/EdoardoTosin/xTetris-Game --depth=1 --single-branch --branch=main xTetris-Game-shallow  
Клонування в "xTetris-Game-shallow"..  
remote: Enumerating objects: 358, done.  
remote: Counting objects: 100% (358/358), done.  
remote: Compressing objects: 100% (226/226), done.  
remote: Total 358 (delta 170), reused 275 (delta 121), pack-reused 0  
Отримання об’єктів: 100% (358/358), 1.11 МіБ | 586.00 КіБ/с, готово.  
Розв’язання дельт: 100% (170/170), готово.
```

#### 2) Показати різницю в розмірі баз даних двох клонів.

```
$ du -sh ./*/.git
3,4M    ./xTetris-Game/.git  
1,4M    ./xTetris-Game-shallow/.git
```

### 2. Зробити не менше трьох локальних комітів.

Створено файл `printSpacing.h` .
```
$ git add printSpacing.h
$ git commit -m "added file printSpacing.h"
[main 39dcea1] added file printSpacing.h  
1 file changed, 14 insertions(+)  
create mode 100644 src/printSpacing.h
```

Змінено файли `printGame.h`, `printGame.c` .
```
$ git commit -am "change dependency"
[main 23eefde] change dependency  
2 files changed, 2 insertions(+), 11 deletions(-)
```

Змінено файли `printGame.h`, `printSpacing.h` .
```
$ git commit -m "removed excess"
[main 8a7338e] removed excess  
2 files changed, 3 insertions(+), 6 deletions(-)
```


```
$ git reset HEAD~1
$ git add printDoubleSpacing.h printDSpacing.h
$ git commit
[main 39fb2e8] renamed printDoubleSpacing.h to printDSpacing.h  
1 file changed, 0 insertions(+), 0 deletions(-)  
rename src/{printDoubleSpacing.h => printDSpacing.h} (100%)
```
![](lab1_2.png)

### 3. Продемонструвати уміння вносити зміни до останнього коміту за допомогою опції --amend.

Змінено файл `printSpacing.h` .
```
$ git add printSpacing.h
$ git commit --amend
[main a1d9fbc] removed excess  
Date: Sun Oct 1 14:26:30 2023 +0300  
2 files changed, 4 insertions(+), 6 deletions(-)
```

### 4. Продемонструвати уміння об'єднати кілька останніх комітів в один за допомогою git reset.

Додано два файли `printDoubleSpacing.h`, `printShortSpacing.h` .
```
$ git add printDoubleSpacing.h printShortSpacing.h
$ git commit -m "added new spacing functions"
[main 66ae32a] added new spacing functions  
2 files changed, 34 insertions(+)  
create mode 100644 src/printDoubleSpacing.h  
create mode 100644 src/printShortSpacing.h  
$ git reset HEAD~2
Неіндексовані зміни після скидання:  
M       src/printGame.h  
M       src/printSpacing.h  
$ git add printGame.h printSpacing.h printDoubleSpacing.h printShortSpacing.h    
$ git commit -m "removed excess and added new spacing functions"  
[main b81c1d1] removed excess and added new spacing functions  
4 files changed, 38 insertions(+), 6 deletions(-)  
create mode 100644 src/printDoubleSpacing.h  
create mode 100644 src/printShortSpacing.h
```

### 5. Видалити файл(и) одним способом на вибір.

```
$ git rm printShortSpacing.h
rm 'src/printShortSpacing.h'  
$ git commit -m "removed printShortSpacing.h"
[main 995a420] removed printShortSpacing.h  
1 file changed, 17 deletions(-)  
delete mode 100644 src/printShortSpacing.h
```

### 6. Перемістити файл(и) одним способом на вибір.

```
$ mv printDoubleSpacing.h printDSpacing.h
$ git add printDoubleSpacing.h printDSpacing.h
$ git commit -m "renamed printDoubleSpacing.h to printDSpacing.h"  
[main 595485c] renamed printDoubleSpacing.h to printDSpacing.h  
1 file changed, 0 insertions(+), 0 deletions(-)  
rename src/{printDoubleSpacing.h => printDSpacing.h} (100%)
```

### 7. Гілкування

```
$ git branch dev-branch-1
$ git checkout -b dev-branch-2
Переключено на нову гілку "dev-branch-2"  
$ git branch dev-branch-3
```

```
$ git checkout dev-branch-1
Переключено на гілку "dev-branch-1"  
$ git mv printSpacing.h printNSpacing.h
$ git commit -m "renamed printSpacing.h to printNSpacing.h"
[dev-branch-1 a2bc194] renamed printSpacing.h to printNSpacing.h  
1 file changed, 0 insertions(+), 0 deletions(-)  
rename src/{printSpacing.h => printNSpacing.h} (100%)
```

```
$ git checkout dev-branch-2
Переключено на гілку "dev-branch-2"  
$ git rm printDSpacing.h
rm 'src/printDSpacing.h'  
$ git commit -m "removed printDSpacing.h"
[dev-branch-2 624ea35] removed printDSpacing.h  
1 file changed, 17 deletions(-)  
delete mode 100644 src/printDSpacing.h
```

```
$ git checkout dev-branch-3
Переключено на гілку "dev-branch-3"  
$ nvim printDSpacing.h
$ git commit -am "change function name in printDSpacing.h"  
[dev-branch-3 7775254] change function name in printDSpacing.h  
1 file changed, 1 insertion(+), 1 deletion(-)
```

```
$ git log --graph --all --format="%h %as '%s'"
* 7775254 2023-10-01 'change function name in printDSpacing.h'  
| * 624ea35 2023-10-01 'removed printDSpacing.h'  
|/     
| * a2bc194 2023-10-01 'renamed printSpacing.h to printNSpacing.h'  
|/     
* 39fb2e8 2023-10-01 'renamed printDoubleSpacing.h to printDSpacing.h'  
* 995a420 2023-10-01 'removed printShortSpacing.h'  
* b81c1d1 2023-10-01 'removed excess and added new spacing functions'  
* 23eefde 2023-10-01 'change dependency'  
* 39dcea1 2023-10-01 'added file printSpacing.h'  
* bc527db 2023-09-26 'Updated documentation'
```

### 8. Продемонструвати уміння знайти в історії комітів набір комітів, в яких була зміна по конкретному шаблону в конкретному файлі.

```
$ git log b81c1d1..HEAD --graph --all --format="%h %as '%s'"
* 7775254 2023-10-01 'change function name in printDSpacing.h'  
| * 624ea35 2023-10-01 'removed printDSpacing.h'  
|/     
| * a2bc194 2023-10-01 'renamed printSpacing.h to printNSpacing.h'  
|/     
* 39fb2e8 2023-10-01 'renamed printDoubleSpacing.h to printDSpacing.h'  
* 995a420 2023-10-01 'removed printShortSpacing.h'
```

```
$ git log -G 'printSpacing' printDSpacing.h
commit 7775254118e296de008b2f5d107b479a19356299 (HEAD -> dev-branch-3)  
Author: Yatskov Maksym <yatskov.maksym@lll.kpi.ua>  
Date:   Sun Oct 1 15:11:10 2023 +0300  
  
   change function name in printDSpacing.h  
  
commit 39fb2e85a00cadaa0947cf91769048bdccbb3b64 (main)  
Author: Yatskov Maksym <yatskov.maksym@lll.kpi.ua>  
Date:   Sun Oct 1 14:59:47 2023 +0300  
  
   renamed printDoubleSpacing.h to printDSpacing.h
```

```
$ git diff 39fb2e85a00cadaa0947cf91769048bdccbb3b64..7775254118e296de008b2f5d107b479a19356299  
diff --git a/src/printDSpacing.h b/src/printDSpacing.h  
index 7ce6bfe..1689699 100644  
--- a/src/printDSpacing.h  
+++ b/src/printDSpacing.h  
@@ -7,7 +7,7 @@  
/*!  
   \brief Print spaces between the two boards.  
*/  
-void printSpacing(){  
+void printDSpacing(){  
     
  int i;
```

```
$ git show 7775254118e296de008b2f5d107b479a19356299 printDSpacing.h    
commit 7775254118e296de008b2f5d107b479a19356299 (HEAD -> dev-branch-3)  
Author: Yatskov Maksym <yatskov.maksym@lll.kpi.ua>  
Date:   Sun Oct 1 15:11:10 2023 +0300  
  
   change function name in printDSpacing.h  
  
diff --git a/src/printDSpacing.h b/src/printDSpacing.h  
index 7ce6bfe..1689699 100644  
--- a/src/printDSpacing.h  
+++ b/src/printDSpacing.h  
@@ -7,7 +7,7 @@  
/*!  
   \brief Print spaces between the two boards.  
*/  
-void printSpacing(){  
+void printDSpacing(){  
     
  int i;
```
